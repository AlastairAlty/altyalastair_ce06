//
//  ViewController2.swift
//  AltyAlastair_CE06
//
//  Created by Alastair Alty on 11/3/20.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var viewColor2: UIView!
    
    @IBOutlet weak var redLabel: UILabel!
    @IBOutlet weak var greenLabel: UILabel!
    @IBOutlet weak var blueLabel: UILabel!
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    // create variables to hold the values from ViewController1
    var redValue = Float()
    var grenValue = Float()
    var blueValue = Float()
    
    @IBAction func sliderChange(_ sender: AnyObject) {
        self.viewColor2.backgroundColor = UIColor(red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: 1.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the sliders to the values of the floats from viewController one
        redSlider.value = redValue
        greenSlider.value = grenValue
        blueSlider.value = blueValue

        // Do any additional setup after loading the view.
        viewColor2.backgroundColor = UIColor(red: CGFloat(redSlider.value), green: CGFloat(greenSlider.value), blue: CGFloat(blueSlider.value), alpha: 1.0)
        
        
        
        
    }
    
    
    
    // function for random color
    @IBAction func randomColor(_ sender: Any) {
        // declare random floats for RGB settings between 0.0 - 1.0
        let redValue = Float.random(in: 0.0...1.0)
        let greenValue = Float.random(in: 0.0...1.0)
        let blueValue = Float.random(in: 0.0...1.0)
        // After creating the the random floats, set the slider values to those random values
        redSlider.value = redValue
        greenSlider.value = greenValue
        blueSlider.value = blueValue
        // change the color of the view to the values of the random floats
        viewColor2.backgroundColor = UIColor(red: CGFloat(redValue), green: CGFloat(greenValue), blue: CGFloat(blueValue), alpha: 1.0)
    }
    
    @IBAction func back(_ sender: Any) {
        self.performSegue(withIdentifier: "Back", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Back" {
            let destination = segue.destination as! ViewController
            destination.redValue = redSlider.value
            destination.greenValue = greenSlider.value
            destination.blueValue = blueSlider.value
    }
        
    }
}
