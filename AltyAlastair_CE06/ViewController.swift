//
//  ViewController.swift
//  AltyAlastair_CE06
//
//  Created by Alastair Alty on 11/3/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var redLabel: UILabel!
    
    @IBOutlet weak var greenLabel: UILabel!
    
    @IBOutlet weak var bluelabel: UILabel!
    
    // Create random floats for RGB scale between 0.0 - 1.0
    var redValue = Float.random(in: 0.0...1.0)
    var greenValue = Float.random(in: 0.0...1.0)
    var blueValue = Float.random(in: 0.0...1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Set the label text to the values of the random floats
        redLabel.text = String(redValue)
        greenLabel.text = String(greenValue)
        bluelabel.text = String(blueValue)
        
        // set the color of the view to the value of the floats for RGB
        colorView.backgroundColor = UIColor(red: CGFloat(redValue), green: CGFloat(greenValue), blue: CGFloat(blueValue), alpha: 1.0)
        
    }
    @IBAction func change(_ sender: Any){
        self.performSegue(withIdentifier: "changeColor", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changeColor" {
            let destination = segue.destination as! ViewController2
            destination.redValue = redValue
            destination.grenValue = greenValue
            destination.blueValue = blueValue
        }
    }
    

}

